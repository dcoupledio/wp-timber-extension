<?php namespace Decoupled\Wordpress\Extension\Timber;

use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationExtension;
use Decoupled\Core\Application\ApplicationContainer;
use Timber\Loader;
use Timber\Post;
use Timber\Menu;
use Timber\User;
use Timber\Timber;

class TimberExtension extends ApplicationExtension{

    protected $timber;

    public function getName()
    {
        return 'timber.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        $this->timber = new Timber();

        self::addServices( $app );

        self::addEvents( $app );
    }

    protected static function addEvents( ApplicationContainer $app )
    {
        $app->when( '$app.run' )->uses(function( $actionQueue, $actionFactory, $template, $app ){

            $app['$template'] = function() use($template){

                return $template;
            };

            $action = $actionFactory->make(function($out, $fileContents, $scope, $template){
                
                $content = $fileContents( $template );

                $template = apply_filters( '$template.legacy', '@app/legacy.html.twig' );

                $scope['content'] = $content;

                return $out( $template )->with( $scope );
            });

            $actionQueue->add( $action );
        });
    }

    protected static function addServices( ApplicationContainer $app )
    {
        $app['$timber.twig'] = function(){

            $timber = new Loader();

            return $timber->get_twig();
        };

        $app->extend('$twig', function( $twig, $app ){

            $twig = $app['$timber.twig'];

            $twig->setLoader( $app['$twig.loader'] );

            return $twig;
        });

        $app->extend('$scope', function( $scope ){

            return $scope->merge( Timber::get_context() );
        });

        $app['$post'] = function(){

            return new Post();
        };

        $app['$posts'] = function(){

            return Timber::get_posts();
        };

        $app['$author'] = function(){

            global $wp_query;

            if( $author = @$wp_query->query_vars['author'] )
            {
                return new User( $author );
            }

            return false;
        };

        $app['$menu'] = function(){

            return function( $id = null ){
                return new Menu($id);
            };
        }; 

        $app['$file.contents'] = function(){

            return function( $file ){

                ob_start();

                require($file);

                return ob_get_clean();
            };
        };

        $app['$archive'] = function($c){

            $archive = new \stdClass();

            $archive->type = false;

            if ( is_day() ) 
            {
                $archive->type  = 'day';

                $archive->date  = get_the_date( 'D M Y' );
            } 
            else if ( is_month() ) 
            {
                $archive->type = 'month';

                $archive->date = get_the_date( 'M Y' ); 
            } 
            else if ( is_year() ) 
            {
                $archive->type = 'year';

                $archive->date = get_the_date( 'Y' );
            } 
            else if ( is_tag() ) 
            {
                $archive->type = 'tag';

                $archive->title = single_tag_title( '', false );
            } 
            else if ( is_category() ) 
            {
                $archive->type = 'category';

                $archive->title = single_cat_title( '', false );
            } 
            else if ( is_post_type_archive() ) 
            {
                $archive->type = get_post_type();

                $archive->title = post_type_archive_title( '', false );
            }

            $archive->posts = $c['$posts'];

            return $archive;  
        };        
    }
}